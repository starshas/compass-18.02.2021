package com.starshas.compass

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.text.Html
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), SensorEventListener {
    private var isActivityInitialized: Boolean = false
    private var initializedWithLocationSettingEnabled: Boolean = false
    private lateinit var mLocationManager: LocationManager

    private var sensorManager: SensorManager? = null
    private val PERMISSION_LOCATION_REQUEST_CODE = 101

    private var degreeStart = 0f

    private var userLatitude: Double? = null
    private var userLongitude: Double? = null
    private var degreeToNorth: Float = 0.0F

    val mLocationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            userLatitude = location.latitude
            userLongitude = location.longitude
        }

        override fun onProviderDisabled(provider: String) {
            try {
                super.onProviderDisabled(provider)
            } catch (e: AbstractMethodError) {
                Toast.makeText(
                    this@MainActivity,
                    "Please enable location setting",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        override fun onProviderEnabled(provider: String) {
            try {
                super.onProviderDisabled(provider)
            } catch (e: AbstractMethodError) {
            }
        }

        override fun onStatusChanged(
            provider: String?,
            status: Int,
            extras: Bundle?
        ) {
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setTitle(Html.fromHtml("<font color='#ffffff'>Compass</font>"));

        setDistanceToDestination(0F)

        buttonSetDestination.getBackground()
            .setColorFilter(getColor(R.color.green), PorterDuff.Mode.MULTIPLY)

        mLocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (locationProvidersDisabled() || !isLocationPermitted()) {
            openLocationSettings()
        } else {
            initializeActivityWithLocationSettingEnabled()
        }
    }

    private fun initializeActivityWithLocationSettingEnabled() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_LOCATION_REQUEST_CODE
            )
            return
        }
        initializeActivityWithPermissions()
        initializedWithLocationSettingEnabled = true
    }

    private fun initializeActivityWithPermissions() {
        initializeLocationManager()
        initializeSensorManager()

        configureButtonSetDestination()
    }

    private fun initializeSensorManager() {
        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
    }

    @SuppressLint("MissingPermission")
    private fun initializeLocationManager() {
        mLocationManager = getSystemService(LOCATION_SERVICE) as LocationManager

        if (!isLocationPermitted() || !isLocationPermitted()) return

        mLocationManager.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER, 0L,
            0f, mLocationListener
        )
    }

    private fun isLocationPermitted(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return false
        }
        return true
    }

    private fun configureButtonSetDestination() {
        buttonSetDestination.setOnClickListener {
            val dialog = Dialog(this)
            val layout = View.inflate(this, R.layout.dialog_set_coordinates, null)
            val editTextLatitude = layout.findViewById<EditText>(R.id.editTextLatitude)
            val editTextLongitude = layout.findViewById<EditText>(R.id.editTextLongitude)
            val buttonOk = layout.findViewById<Button>(R.id.buttonOk)
            buttonOk.setOnClickListener {
                val latitude = editTextLatitude.text.toString().toDoubleOrNull()
                val longitude = editTextLongitude.text.toString().toDoubleOrNull()

                var isDataCorrect = true
                if (latitude == null || latitude < -90 || latitude > 90) {
                    editTextLatitude.setHint("Wrong latitude!")
                    isDataCorrect = false
                }
                if (longitude == null || longitude < -180 || longitude > 180) {
                    editTextLongitude.setHint("Wrong longitude!")
                    isDataCorrect = false
                }

                if (isDataCorrect) {
                    selectedLatitude = latitude!!
                    selectedLongitude = longitude!!
                    dialog.cancel()
                }
            }
            buttonOk.getBackground()
                .setColorFilter(getColor(R.color.green), PorterDuff.Mode.MULTIPLY)
            dialog.setContentView(layout)
            dialog.show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_LOCATION_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            initializeActivityWithPermissions()
        else
            finish()
    }

    override fun onPause() {
        super.onPause()
        sensorManager?.unregisterListener(this)
    }

    override fun onResume() {
        super.onResume()

        sensorManager?.registerListener(
            this, sensorManager!!.getDefaultSensor(Sensor.TYPE_ORIENTATION),
            SensorManager.SENSOR_DELAY_GAME
        )

        mLocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (isActivityInitialized) {
            if (locationProvidersDisabled()) {
                openLocationSettings()
            } else {
                if (!initializedWithLocationSettingEnabled) {
                    initializeActivityWithLocationSettingEnabled()
                }
            }
        }
        isActivityInitialized = true
    }

    private fun locationProvidersDisabled() =
        !mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                !mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

    private fun openLocationSettings() {
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivity(intent)
        Toast.makeText(this, "Enable location settings", Toast.LENGTH_SHORT).show()
    }

    //Defaults are North Pole Coordinates
    private var selectedLatitude = 90.0
    private var selectedLongitude = 0.0

    override fun onSensorChanged(event: SensorEvent) {
        degreeToNorth = Math.round(event.values[0]).toFloat()
        val compassRotateAnimation = RotateAnimation(
            degreeStart,
            -degreeToNorth,
            Animation.RELATIVE_TO_SELF, 0.5f,
            Animation.RELATIVE_TO_SELF, 0.5f
        )
        compassRotateAnimation.fillAfter = true
        compassRotateAnimation.duration = 200
        compassImage.startAnimation(compassRotateAnimation)
        degreeStart = -degreeToNorth

        if (userLatitude != null && userLongitude != null) {
            val locationUser = Location("User location")

            locationUser.latitude = userLatitude!!
            locationUser.longitude = userLongitude!!

            val locationSelected = Location("Selected location")

            locationSelected.latitude = selectedLatitude
            locationSelected.longitude = selectedLongitude

            val distance = locationUser.distanceTo(locationSelected)
            val angleGoTo = locationUser.bearingTo(locationSelected)

            setGoToDirection(angleGoTo)

            setDistanceToDestination(distance)
        }
    }

    private fun setDistanceToDestination(distance: Float) {
        buttonDistanceFromDestination.setText(
            getString(
                R.string.distance_to_the_destination_m,
                distance.toString()
            )
        )
    }

    private fun setGoToDirection(degree: Float) {
        arrowGoToDirection.rotation = (360 - degreeToNorth) + degree
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
    }
}